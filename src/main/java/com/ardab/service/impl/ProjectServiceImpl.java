package com.ardab.service.impl;

import com.ardab.dto.ProjectDto;
import com.ardab.entity.Company;
import com.ardab.entity.Project;
import com.ardab.repository.CompanyRepository;
import com.ardab.repository.ProjectRepository;
import com.ardab.service.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProjectServiceImpl implements ProjectService {
    private final ProjectRepository projectRepository;
    private final CompanyRepository companyRepository;

    @Override
    @Transactional
    public ProjectDto save(ProjectDto projectDto){
        Assert.notNull(projectDto.getProjectName(),"Required Field!!!");
        Project project = new Project();
        project.setProjectName(projectDto.getProjectName());
        project.setProjectNumber(projectDto.getProjectNumber());
        project.setProjectOwner(projectDto.getProjectOwner());
        project.setProjectPrice(projectDto.getProjectPrice());
        final Project projectDb = projectRepository.save(project);

        List<Company> companyList = new ArrayList<>();
        projectDto.getCompanies().forEach(item -> {
            Company company = new Company();
            company.setCompanyName(item);
            companyList.add(company);
        });
        companyRepository.saveAll(companyList);
        projectDto.setId(projectDb.getId());
        return projectDto;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public List<ProjectDto> getAll() {
        List<Project> projects = projectRepository.findAll();
        List<ProjectDto> projectDtos = new ArrayList<>();

        projects.forEach(it -> {
            ProjectDto projectDto = new ProjectDto();
            projectDto.setId(it.getId());
            projectDto.setProjectName(it.getProjectName());
            projectDto.setProjectPrice(it.getProjectPrice());
            projectDto.setProjectNumber(it.getProjectNumber());
            projectDto.setProjectPrice(it.getProjectPrice());
            projectDto.setCompanies(
                    it.getCompanyList() != null ?
                            it.getCompanyList().stream().map(Company::getCompanyName).collect(Collectors.toList()) : null);
            projectDtos.add(projectDto);
        });
        return projectDtos;

    }

    @Override
    public Page<ProjectDto> getAll(Pageable pageable) {
        return null;
    }
}
