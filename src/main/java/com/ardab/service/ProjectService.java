package com.ardab.service;

import com.ardab.dto.ProjectDto;
import org.springframework.data.domain.Page;

import java.awt.print.Pageable;
import java.util.List;

public interface ProjectService {

    ProjectDto save(ProjectDto projectDto);

    void delete(Long id);

    List<ProjectDto> getAll();

    Page<ProjectDto> getAll(Pageable pageable);
}
