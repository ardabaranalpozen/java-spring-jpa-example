package com.ardab.dto;

import com.sun.istack.NotNull;
import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(of = {"id"})
public class ProjectDto {
    private Long id;

    @NotNull
    private String projectName;

    @NotNull
    private int projectNumber;

    private String projectOwner;

    private double projectPrice;

    private List<String> companies;
}
