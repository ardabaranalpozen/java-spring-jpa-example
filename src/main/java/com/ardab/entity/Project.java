package com.ardab.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "projects")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
@ToString
public class Project {

    @Id
    @SequenceGenerator(name = "seq_project", allocationSize = 1)
    @GeneratedValue(generator = "seq_project", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "project_name")
    private String projectName;

    @Column(name = "project_no")
    private int projectNumber;

    @Column(name = "project_owner")
    private String projectOwner;

    @Column(name = "price")
    private double projectPrice;

    @OneToMany
    @JoinColumn(name = "company_id")
    private List<Company> companyList;

}
