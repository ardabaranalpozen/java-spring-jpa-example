package com.ardab.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name="company")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Company {

    @Id
    @SequenceGenerator(name = "seq_company", allocationSize = 1)
    @GeneratedValue(generator = "seq_company", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "country")
    private String country;

}
