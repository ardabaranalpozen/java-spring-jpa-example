package com.ardab.controller;

import com.ardab.dto.ProjectDto;
import com.ardab.service.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/project")
@RequiredArgsConstructor
public class ProjectController {

    private final ProjectService projectService;

    @PostMapping
    public ResponseEntity<ProjectDto> save(@Validated @RequestBody ProjectDto projectDto){
        return ResponseEntity.ok(projectService.save(projectDto));
    }

    @GetMapping
    public ResponseEntity<List<ProjectDto>> listAll() {
        return ResponseEntity.ok(projectService.getAll());
    }

}
